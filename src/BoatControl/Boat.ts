import { Coordinate } from "./Coordinate";

export class Boat {

    private speedInMsPerPixel = 5;

    //The current location of the boat
    private location: Coordinate;

    //Is the boat allowed to drive
    private driveAllowance: boolean = false;

    //Promise that is resolved when the boat is done driving 
    //Either because it reached it's target, or because it was stopped
    private drivingDone = Promise.all([]);;

    //The pixels the boat traveled.
    private pixelsTraveled: number = 0;

    //Called when picelsTraveled is updated
    public $onTravelPixel: (pixelsTraveled: number) => void;

    //Called when a station is reached
    public $onStationReached: (stationName: string) => void;

    //Called when the boat starts or stops
    public $onStartStop: (driving: number) => void;


    constructor(private element: HTMLElement) {
        element.style.setProperty('position', 'absolute');
    }

    setPosition = (coordinate: Coordinate) => {
        this.location = coordinate;
        this.element.style.left = (coordinate.x - this.element.clientWidth / 2).toString();
        this.element.style.top = (coordinate.y - this.element.clientHeight / 2).toString();
    }


    /**
     * The moveX and moveY functions are very similar.
     * One could merge them into a single function using more parameters,
     * but as they are rather complex methods, it would probalby reduce the  readability.
     */
    private moveX = (resolve: Function, target: number) => {
        let direction = this.location.x - target < 0 ? 1 : -1;                          //calculate direction of the boat
        window.setTimeout(() => {                                                       //move one pixel every five s
            let nextX: number = this.location.x + direction;                            //set next position
            this.setPosition(new Coordinate(nextX, this.location.y));
            if (this.$onTravelPixel) { this.$onTravelPixel(++this.pixelsTraveled) };    //update pixels traveled
            if (this.driveAllowance && this.location.x !== target) {
                this.moveX(resolve, target);                                            //if destination not yet reached, call method recursively
            } else {
                resolve();                                                              //Otherwise resolve the promise
            }
        }, this.speedInMsPerPixel);
    }

    private moveY = (resolve: Function, target: number) => {
        let direction = this.location.y - target < 0 ? 1 : -1;
        window.setTimeout(() => {
            let nextY: number = this.location.y + direction;
            this.setPosition(new Coordinate(this.location.x, nextY));
            if (this.$onTravelPixel) { this.$onTravelPixel(++this.pixelsTraveled) };
            if (this.driveAllowance && this.location.y !== target) {
                this.moveY(resolve, target);
            } else {
                resolve();
            }
        }, this.speedInMsPerPixel);
    }


    private driveToCoordinate = (coordinate: Coordinate) => {
        console.debug(`Driving boat to location: ${coordinate.x}:${coordinate.y}`);
        console.debug(`Drive allowance: ${this.driveAllowance}`);
        console.debug(`Gradient to target = ${this.location.gradientTo(coordinate)}`);

        //Move both coordinates independent from each other. 
        //Create a promise that will be resolved when a coordinate is reached.
        //This way the application stays reactive
        let xReached = new Promise((resolve) => this.moveX(resolve, coordinate.x));
        let yReached = new Promise((resolve) => this.moveY(resolve, coordinate.y));

        //Another promise that is resolved when both coordinates are reached
        this.drivingDone = Promise.all([xReached, yReached]);

        //Update kpi infos when boat reaches a station
        this.drivingDone.then(onfulfilled => {
            if (this.location.isEqualTo(coordinate)) {
                this.updateKPIsOnHalt(coordinate.name);
            }
        });
        return this.drivingDone;
    }

    private updateKPIsOnHalt = (stationName: string) => {
        if (this.$onStationReached) {
            if (stationName) {
                this.$onStationReached(stationName);
                if (this.$onStartStop) { this.$onStartStop(0) };
            }
        }
    }

    //Call the travel method recursive to travel to each
    //station of a route in the order in which they are in the array
    private travelRoute = (route: Coordinate[]) => {
        if (this.driveAllowance) {
            if (route.length > 1) {
                this.driveToCoordinate(route.shift()).then(() => this.travelRoute(route));
            } else {
                this.driveToCoordinate(route.shift()).then(() => this.driveAllowance = false);
            }
        }
    }

    public driveTo = (calculateRoute: () => Coordinate[]) => {
        this.driveAllowance = true;
        if (this.$onStartStop) { this.$onStartStop(1) };
        this.travelRoute(calculateRoute());
    }

    public stop = () => {
        this.driveAllowance = false;
        return this.drivingDone;
    }

    public getLocation = () => {
        return this.location
    }


}
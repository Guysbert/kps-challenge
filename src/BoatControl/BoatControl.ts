import { Boat } from "./Boat";
import { Lake } from "./Lake";
import { KPI } from "./KPI";
import { Station } from "./Station";
import { Button } from "./Button";
import { Coordinate } from "./Coordinate";

export enum RoutingMethod {
    DIRECT_ROUTE, GUIDED_ROUTE
}

export class BoatControl {

    private boat: Boat;
    private lake: Lake;
    private stations: Station[];
    private buttons: Button[];
    private distanceKpi: KPI;
    private lastStationKpi: KPI;
    private movingKpi: KPI;

    constructor(private routingMethod: RoutingMethod) {
        this.linkElementsFromDom();
        this.linkButtonsToBoat();
        this.linkKPIsToBoat();
    }

    public init = () => {
        this.boat.setPosition(this.lake.getCenter());
    }

    private linkElementsFromDom = () => {
        //Loads all elements from dom and links them to javascript variables.
        this.boat = new Boat(document.getElementById('boat'));
        this.lake = new Lake(document.getElementById('lake'));
        this.linkButtons();
        this.linkStations();
        this.linkKPIs();
    }

    private linkStations = () => {        
        this.stations = [];
        let domStations: HTMLCollectionOf<Element> = document.getElementsByClassName('station');
        for (let i = 0; i < domStations.length; i++) {
            this.stations.push(new Station(<HTMLDivElement>domStations.item(i)));
        }
    }

    private linkButtons = () => {
        this.buttons = [];
        let domButtons: HTMLCollectionOf<Element> = document.getElementsByClassName('navigate');
        for (let i = 0; i < domButtons.length; i++) {
            this.buttons.push(new Button(<HTMLButtonElement>domButtons.item(i)));
        }
    }

    private linkKPIs = () => {
        this.distanceKpi = new KPI(document.getElementById('travel'));
        this.lastStationKpi = new KPI(document.getElementById('last-station'));
        this.movingKpi = new KPI(document.getElementById('moving'));
    };

    private calculateRoute = (target: Coordinate): Coordinate[] => {
        // If we're using DIRECT_ROUTE mode, travel directly to the target.
        if (this.routingMethod === RoutingMethod.DIRECT_ROUTE){
            return [target];
        } else {
            /**
             * If the targets are reachable on horizontal or vertical line,
             * the gradient between boat and target is either 0 or +/- Infinity.
             * Then we can define a route directly to the target.
             * Otherwise we must first travel to the center of the lake,
             * so the boat keeps inside the bouys.
             */
            let routeGradient = Math.abs(this.boat.getLocation().gradientTo(target));    
            if (routeGradient === 0 || routeGradient === Infinity) {
                return [target];
            } else {
                return [this.lake.getCenter(), target];
            }
        }        
    }

    private linkButtonsToBoat = () => {
        //link the drive function to the buttons. Fortunately the button index is the same as the station index.
        //Therefore we don't need to reference the content of the buttons.
        this.buttons.forEach((button, index) => {
            button.setOnClick(() => {
                this.boat.stop().then(() => this.boat.driveTo(() => this.calculateRoute(this.stations[index].getLocation())));
            })
        });
    }

    private linkKPIsToBoat = () => {
        this.boat.$onStartStop = (driving: number) => this.movingKpi.setText(driving.toString());
        this.boat.$onTravelPixel = (pixelsTraveled: number) => this.distanceKpi.setText(pixelsTraveled.toString() + 'px');
        this.boat.$onStationReached = (stationName: string) => this.lastStationKpi.setText(stationName);
    }
}